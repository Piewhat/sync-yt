FROM rust:latest

WORKDIR /usr/src/sync-yt
COPY . .

RUN cargo install --path .

CMD ["sync-yt"]