use chrono::{DateTime, NaiveDateTime, Utc};
use std::env;
use tokio::sync::mpsc::UnboundedSender;
use warp::ws::Message;

use crate::{youtube::List, Room, Update};

pub enum Event {
    Join,
    Ready,
    Play(f64),
    Pause(f64),
    Seek(f64),
    VideoUpdate(String),
    VideoSearch(String),
    Message(String),
    NameUpdate(String),
    OwnerUpdate(usize),
    UserState(i8),
    RoomNameUpdate(String),
    RoomPermissionUpdate(String),
}

impl Event {
    pub async fn run(
        self,
        my_id: usize,
        room: &mut Room,
        update: &mut Update,
        tx: &UnboundedSender<warp::filters::ws::Message>,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let mut user = match room.users.get_mut(&my_id) {
            Some(user) => user,
            None => return Err("user not found".into()),
        };

        match self {
            Event::VideoSearch(q) => {
                if q.is_empty() {
                    return Err("search query empty".into());
                }

                println!("video search in room: {} q: {}", &room.id, q);
                let key = env::var("YT").expect("Expected a YouTube key in the environment");
                let body = reqwest::get(&format!("https://www.googleapis.com/youtube/v3/search?part=snippet,id&order=relevance&q={}&safeSearch=none&type=video&key={}", q, key))
                    .await?
                    .json::<List>()
                    .await?;

                update.event = "video search".into();
                update.set(&body.items);

                tx.send(Message::text(serde_json::to_string(&update)?))?;

                return Ok(());
            }
            Event::VideoUpdate(video_id) => {
                println!("video update in room: {} video id: {}", &room.id, video_id);
                room.video_buffering = true;
                room.video_id = video_id;
                room.timestamp = update.timestamp;
                room.time = 0.0;
                room.paused = false;
                room.last_update = update.clone();

                for (_, user) in room.users.iter_mut() {
                    user.ready = false;
                }
            }
            Event::Seek(time) => {
                println!("seek in room: {} time: {}", &room.id, time);
                room.time = time;
                room.timestamp = update.timestamp;
                room.video_buffering = true;
                room.last_update = update.clone();

                for (_, user) in room.users.iter_mut() {
                    user.ready = false;
                }
            }
            Event::Message(msg) => {
                println!("message in room: {} msg: {}", &room.id, msg);
                update.message = msg;
            }
            Event::Pause(time) => {
                println!("pause in room: {}", &room.id);
                room.paused = true;
                room.time = time;
                room.timestamp = update.timestamp;
            }
            Event::Play(time) => {
                println!("play in room: {}", &room.id);
                room.paused = false;
                room.time = time;
                room.timestamp = update.timestamp;
            }
            Event::Join => {
                room.viewers += 1;
                room.sync_next = (true, my_id);
                update.set(&room.users);

                println!("join in room: {} viewers: {}", &room.id, room.viewers);

                tx.send(Message::text(serde_json::to_string(&room)?))?
            }
            Event::Ready => {
                println!("user {} is ready in room {}", my_id, room.id);

                if room.video_buffering {
                    user.ready = true;
                    let not_ready = room.users.values().any(|u| !u.ready);
                    if !not_ready {
                        println!("room is ready {}", room.id);
                        update.message = "ready".into();
                        room.video_buffering = false;
                        room.last_update.timestamp = update.timestamp;

                        for (_, user) in room.users.iter_mut() {
                            user.ready = false;
                        }
                    }
                }
            }
            Event::NameUpdate(name) => {
                user.name = Some(name);
                update.set(&user);
            }
            Event::OwnerUpdate(uid) => {
                room.owner = uid;
                update.message = uid.to_string();
            }
            Event::UserState(state) => {
                let naive =
                    NaiveDateTime::from_timestamp_opt(room.timestamp / 1000, 0).unwrap_or_default();
                let datetime: DateTime<Utc> = DateTime::from_utc(naive, Utc);
                let duration = Utc::now().signed_duration_since(datetime);
                let mut time = room.time;

                user.state = state;

                if !room.paused {
                    time = duration.num_seconds() as f64 + room.time;
                }

                update.set(&room.users.get(&my_id));

                if state == 1 && room.sync_next.0 {
                    room.sync_next.0 = false;

                    let _ = room
                        .send_update(&Update {
                            id: room.id.clone(),
                            event: "seek".into(),
                            message: time.to_string(),
                            timestamp: room.timestamp,
                            uid: 0,
                        })
                        .await;
                }
            }
            Event::RoomNameUpdate(name) => {
                room.name = name;
                println!("room name update in {} room name {}", room.id, room.name);
            }
            Event::RoomPermissionUpdate(perm) => {
                match perm.as_str() {
                    "video" => room.perms.video_update = !room.perms.video_update,
                    "seek" => room.perms.seek = !room.perms.seek,
                    "playpause" => room.perms.play_pause = !room.perms.play_pause,
                    e => return Err(format!("unknown permission: {}", e).into()),
                };

                update.set(&room.perms);

                println!("room permission update in {} permission {}", room.id, perm);
            }
        }

        room.send_update(update).await?;

        Ok(())
    }
}
