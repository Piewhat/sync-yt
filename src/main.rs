use event::Event;
use futures::StreamExt;
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    str::FromStr,
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc,
    },
    time::Duration,
};
use tokio::sync::{
    mpsc::{self, UnboundedSender},
    RwLock,
};
use tokio_stream::wrappers::UnboundedReceiverStream;
use warp::{
    http::Uri,
    ws::{Message, WebSocket},
    Filter,
};

mod event;
mod youtube;

type Rooms = Arc<RwLock<HashMap<String, Room>>>;
type Users = Arc<RwLock<HashMap<usize, String>>>;

static NEXT_USER_ID: AtomicUsize = AtomicUsize::new(1);

#[derive(Serialize, Deserialize, Clone)]
pub struct Room {
    id: String,
    last_update: Update,
    name: String,
    owner: usize,
    paused: bool,
    perms: Perms,
    sync_next: (bool, usize),
    time: f64,
    timestamp: i64,
    video_buffering: bool,
    video_id: String,
    viewers: i32,
    users: HashMap<usize, User>,
}

#[derive(Serialize, Deserialize, Clone)]
struct User {
    name: Option<String>,
    state: i8,
    ready: bool,
    #[serde(skip)]
    conn: Option<mpsc::UnboundedSender<Message>>,
}

#[derive(Serialize, Deserialize, Clone, Default)]
struct Perms {
    video_update: bool,
    play_pause: bool,
    seek: bool,
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Update {
    id: String,
    event: String,
    message: String,
    timestamp: i64,
    uid: usize,
}

impl Room {
    fn new(id: &str, owner_id: usize) -> Self {
        Room {
            id: id.to_string(),
            last_update: Update::default(),
            name: format!("Room {}", id),
            owner: owner_id,
            paused: false,
            perms: Perms::default(),
            sync_next: (false, 0),
            time: 0.0,
            timestamp: 0,
            video_buffering: false,
            video_id: "".into(),
            viewers: 0,
            users: HashMap::default(),
        }
    }

    async fn send_update(&self, update: &Update) -> Result<(), Box<dyn std::error::Error>> {
        let json = serde_json::to_string(&update)?;

        for user in self.users.values() {
            if let Some(conn) = &user.conn {
                conn.send(Message::text(&json))?;
            }
        }

        Ok(())
    }
}

impl User {
    fn new(conn: mpsc::UnboundedSender<Message>) -> User {
        User {
            name: None,
            state: 0,
            ready: false,
            conn: Some(conn),
        }
    }
}

impl Update {
    fn set(&mut self, msg: impl Serialize) {
        if let Ok(json) = serde_json::to_string(&msg) {
            self.message = json;
        }
    }
}

#[tokio::main]
async fn main() {
    let rooms = Rooms::default();
    let users = Users::default();
    let rooms = warp::any().map(move || rooms.clone());
    let users = warp::any().map(move || users.clone());

    let code = warp::path::end().map(|| {
        warp::redirect::temporary(Uri::from_str(&format!("/room?id={}", gen_id())).unwrap())
    });
    let index = warp::path("room").and(warp::fs::file("./www/index.html"));
    let index_css = warp::path!("index.css").and(warp::fs::file("./www/index.css"));
    let scripts = warp::path("scripts").and(warp::fs::dir("./www/scripts"));

    let chat = warp::path("chat")
        .and(warp::ws())
        .and(rooms)
        .and(users)
        .map(|ws: warp::ws::Ws, rooms, users| {
            ws.on_upgrade(move |socket| socket_connected(socket, rooms, users))
        });

    let routes = warp::get().and(code.or(chat).or(index).or(index_css).or(scripts));
    warp::serve(routes).run(([0, 0, 0, 0], 3030)).await;
}

async fn socket_connected(ws: WebSocket, rooms: Rooms, users: Users) {
    let uid = NEXT_USER_ID.fetch_add(1, Ordering::Relaxed);
    let (room_ws_tx, mut room_ws_rx) = ws.split();
    let (tx, rx) = mpsc::unbounded_channel();
    let rx = UnboundedReceiverStream::new(rx);

    let ping_to_ws = ping(tx.clone());
    let to_ws = rx.map(Ok).forward(room_ws_tx);
    let ws_to = async move {
        while let Some(message) = room_ws_rx.next().await {
            if let Err(err) = message_handler(&tx, uid, message, &rooms, &users).await {
                eprintln!("{}", err);
            };
        }

        (uid, rooms, users)
    };

    tokio::pin!(to_ws, ws_to, ping_to_ws);
    tokio::select! {
        (uid, rooms, users) = ws_to => {
            if let Err(error) = user_disconnected(uid, &rooms, &users).await {
                eprintln!("{}", error);
            }
        },
        _ = to_ws => {},
        _ = ping_to_ws => {},
    }
}

async fn ping(tx: UnboundedSender<Message>) {
    loop {
        if let Err(err) = tx.send(Message::ping([0x9])) {
            eprint!("{}", err);
        }
        tokio::time::sleep(Duration::from_secs(5)).await;
    }
}

async fn message_handler(
    tx: &UnboundedSender<Message>,
    uid: usize,
    msg: Result<Message, warp::Error>,
    rooms: &Rooms,
    users: &Users,
) -> Result<(), Box<dyn std::error::Error>> {
    let msg = msg?;
    if !msg.is_text() {
        return Ok(());
    }

    let mut update: Update = serde_json::from_str(msg.to_str().unwrap_or_default())?;
    update.uid = uid;
    let message = &update.message;
    let event = match update.event.as_str() {
        "video search" => Event::VideoSearch(message.to_string()),
        "video update" => Event::VideoUpdate(message.to_string()),
        "seek" => Event::Seek(message.parse()?),
        "pause" => Event::Pause(message.parse()?),
        "play" => Event::Play(message.parse()?),
        "join" => Event::Join,
        "ready" => Event::Ready,
        "message" => Event::Message(message.to_string()),
        "name update" => Event::NameUpdate(message.to_string()),
        "owner update" => Event::OwnerUpdate(message.parse()?),
        "user state" => Event::UserState(message.parse()?),
        "room name update" => Event::RoomNameUpdate(message.to_string()),
        "room permission update" => Event::RoomPermissionUpdate(message.to_string()),
        e => return Err(format!("unknown event: {}", e).into()),
    };

    let new_room = Room::new(&update.id, uid);
    let mut rooms_lock = rooms.write().await;

    if rooms_lock.get(&update.id).is_none() {
        rooms_lock.insert(update.id.clone(), new_room);
    }

    let room = match rooms_lock.get_mut(&update.id) {
        Some(room) => room,
        None => return Err("room not found".into()),
    };

    {
        let mut users_lock = users.write().await;
        users_lock.insert(uid, room.id.clone());
    }

    room.users
        .entry(uid)
        .or_insert_with(|| User::new(tx.clone()));

    let allowed_event = match event {
        Event::VideoUpdate(_) => room.perms.video_update,
        Event::Seek(_) => room.perms.seek,
        Event::Pause(_) => room.perms.play_pause,
        Event::Play(_) => room.perms.play_pause,
        Event::RoomNameUpdate(_) => room.owner == uid,
        Event::RoomPermissionUpdate(_) => room.owner == uid,
        Event::OwnerUpdate(_) => false,
        _ => true,
    };

    if uid == room.owner || allowed_event {
        event.run(uid, room, &mut update, tx).await?;
    } else {
        println!(
            "owner event from nonowner in room: {} event: {}",
            &room.id,
            update.event.as_str()
        );
    }

    Ok(())
}

async fn user_disconnected(
    id: usize,
    rooms: &Rooms,
    users: &Users,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut users_lock = users.write().await;
    let user = users_lock.remove(&id).ok_or("can't find user")?;

    let mut rooms_lock = rooms.write().await;
    {
        let room = rooms_lock.get_mut(&user).ok_or("can't find room")?;
        room.viewers -= 1;
        room.users.remove(&id);

        let not_ready = room.users.values().any(|u| !u.ready);
        let mut send_last = false;

        if !not_ready && room.video_buffering {
            room.video_buffering = false;
            send_last = true;
            println!("sending last update in room {}.", room.id);
        }

        if id == room.owner && !room.users.is_empty() {
            let new_owner = *room.users.keys().next().ok_or("can't find user")?;
            room.owner = new_owner;
            room.send_update(&Update {
                id: room.id.clone(),
                event: "owner update".into(),
                message: new_owner.to_string(),
                timestamp: room.timestamp,
                uid: id,
            })
            .await?;
        }

        if send_last {
            room.send_update(&room.last_update).await?;
        }

        room.send_update(&Update {
            id: room.id.clone(),
            event: "leave".into(),
            message: id.to_string(),
            timestamp: room.timestamp,
            uid: id,
        })
        .await?;

        println!(
            "user {} has left {} with {} viewers.",
            id, room.id, room.viewers
        );
    }

    let rooms_lock_clone = rooms_lock.clone();
    let room = rooms_lock_clone.get(&user).ok_or("can't find room")?;

    if room.users.is_empty() {
        rooms_lock.remove(&room.id);
        println!("room {} removed.", room.id);
    }

    Ok(())
}

fn gen_id() -> String {
    let mut rng = thread_rng();
    std::iter::repeat(())
        .map(|()| rng.sample(Alphanumeric))
        .map(char::from)
        .take(7)
        .collect()
}
