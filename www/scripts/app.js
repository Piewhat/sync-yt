let gotFirstUpdate = false,
    myjoin = true,
    username,
    videoChanged = false,
    lastTime = 0,
    my_id,
    owner,
    perms,
    users = {},
    lastVolume = 100,
    lastSearch,
    searchCache = new Map();

let url = new URL(window.location.href);
let id = url.searchParams.get("id");

function ws() {
    //let socket = new WebSocket("wss://domain/chat");
    let socket = new WebSocket("ws://localhost:3030/chat");
    socket.onopen = () => {
        send(socket, id, "join", "");
        regControls(socket);
    };

    socket.onmessage = (e) => event_handler(JSON.parse(e.data), socket);

    player.addEventListener("onStateChange", playerStateChange);

    function playerStateChange(event) {
        send(socket, id, "user state", event.data.toString());
        let progressBar = document.querySelector("#progress-bar");
        let playBtn = document.querySelector("#play-icon");
        let pauseBtn = document.querySelector("#pause-icon");
        console.log(event);
        if (event.data == 1) {
            let newTime = player.getDuration() * (progressBar.value / 100);
            send(socket, id, "play", newTime.toString());

            playBtn.style.display = "none";
            pauseBtn.style.display = "inline";

        } else if (event.data == 2) {
            let newTime = player.getDuration() * (progressBar.value / 100);
            send(socket, id, "pause", newTime.toString());

            pauseBtn.style.display = "none";
            playBtn.style.display = "inline";
        }


        if (event.data == YT.PlayerState.PAUSED && !paused) {
            // player.playVideo();
        } else if (event.data == YT.PlayerState.PLAYING && paused) {
            // player.pauseVideo();
        }
        if (event.data == YT.PlayerState.PLAYING && videoChanged) {
            send(socket, id, "ready", "");
            videoChanged = false;
        }
    }

    function event_handler(update, socket) {
        log(`Got [${update.event}] event from ${getName(update.uid)}.`);
        let viewers = document.querySelector("#viewers");
        let roomName = document.querySelector("#room-name");

        switch (update.event) {
            case "join":
                if (myjoin) {
                    myjoin = false;
                    if (my_id == owner) {
                        let defaultPerms = JSON.parse(localStorage.getItem("default perms"));

                        if (!defaultPerms) {
                            return
                        }

                        if (defaultPerms.video_update) {
                            send(socket, id, "room permission update", "video");
                        }
                        if (defaultPerms.seek) {
                            send(socket, id, "room permission update", "seek");
                        }
                        if (defaultPerms.play_pause) {
                            send(socket, id, "room permission update", "playpause");
                        }
                    }
                    return;
                }
                addUsers(JSON.parse(update.message), socket);
                viewers.textContent++;

                break;

            case "leave":
                rmUser(update.message);
                viewers.textContent--;
                break;

            case "ready":
                if (update.message == "ready") {
                    player.seekTo(lastTime);
                }
                break;

            case "play":
                paused = false;
                player.playVideo();
                playpause("pause");
                addStatusMessage(`${getName(update.uid)} unpaused.`);
                break;

            case "pause":
                paused = true;
                player.pauseVideo();
                playpause("play");
                addStatusMessage(`${getName(update.uid)} paused.`);
                break;

            case "seek":
                lastTime = update.message;
                videoChanged = true;
                player.seekTo(update.message, true);
                if (update.uid != 0) {
                    addStatusMessage(`${getName(update.uid)} changed video time.`);
                }
                break;

            case "name update":
                updateUser(JSON.parse(update.message), update.uid);
                break;

            case "video update":
                paused = false;
                videoChanged = true;
                lastTime = 0;
                setVideo(update.message);
                playpause("pause");
                addStatusMessage(`${getName(update.uid)} changed the video.`);
                break;

            case "video search":
                listItems(JSON.parse(update.message), socket);
                break;

            case "message":
                addMessage(update.message);
                break;

            case "owner update":
                owner = update.message;
                addStatusMessage(`${getName(owner)} is now the room owner.`);
                break;

            case "user state":
                let user = JSON.parse(update.message);
                displayUserState(user, update.uid);
                break;

            case "room name update":
                roomName.textContent = update.message;
                break;

            case "room permission update":
                let newPerms = JSON.parse(update.message);

                if (newPerms.video_update != perms.video_update) {
                    addStatusMessage(`Video Select ${newPerms.video_update ? "Everyone": "Owner"}`);
                }

                if (newPerms.seek != perms.seek) {
                    addStatusMessage(`Video Scrubbing ${newPerms.seek ? "Everyone": "Owner"}`);
                }

                if (newPerms.play_pause != perms.play_pause) {
                    addStatusMessage(`Play/Pause ${newPerms.play_pause ? "Everyone": "Owner"}`);
                }

                if (my_id == owner) {
                    localStorage.setItem("default perms", JSON.stringify(newPerms));
                }

                perms = newPerms;
                break;

            default:
                lastRoomState = update;
                log(`viewers: ${update.viewers}`);
                viewers.textContent = update.viewers;
                if (!gotFirstUpdate) {
                    my_id = Object.keys(update.users).slice(-1).pop();
                }

                perms = update.perms;
                users = update.users;

                addUsers(update.users, socket);

                if (gotFirstUpdate) {
                    return;
                }
                owner = update.owner;
                roomName.textContent = update.name;

                let t = update.time;
                if (update.timestamp && !update.paused) {
                    let offset = (new Date() - parseInt(update.timestamp)) / 1000;
                    t = t + offset;
                    log(`offset: ${offset} time: ${update.time} t: ${t}`);
                }

                if (update.paused) {
                    log(`t: ${t}`);
                    paused = true;
                } else {
                    playpause("pause");
                }

                if (update.video_buffering) {
                    videoChanged = true;
                }

                if (update.video_id && update.video_id != player.video_id) {
                    setVideo(update.video_id, t);
                    log("video set");
                }

                gotFirstUpdate = true;

                break;
        }
    }
}

function regControls(socket) {
    let progressBar = document.querySelector("#progress-bar");
    let volumeBar = document.querySelector("#volume-bar");
    let messageBox = document.querySelector("#message");
    let chatSettings = document.querySelector("#settings");
    let settingsBtn = document.querySelector("#settings-button")
    let settingsSaveBtn = document.querySelector("#settings-save-button");
    let usernameInput = document.querySelector("#username");
    let roomName = document.querySelector("#room-name");
    let roomNameBtn = document.querySelector("#room-name-btn");
    let chatUpdates = document.querySelector("#chat-updates");
    let videoPermBtn = document.querySelector("#video-perm");
    let timePermBtn = document.querySelector("#time-perm");
    let playpausePermBtn = document.querySelector("#playpause-perm");
    let videoQuery = document.querySelector("#video-query");
    let videoSearchBtn = document.querySelector("#video-search");
    let searchResults = document.querySelector("#search-results");
    let playBtn = document.querySelector("#play-icon");
    let pauseBtn = document.querySelector("#pause-icon");
    let volumeUpBtn = document.querySelector("#volup");
    let volumeMuteBtn = document.querySelector("#volmute");

    let localUsername = localStorage.getItem("username");
    if (localUsername) {
        username = localUsername;
        send(socket, id, "name update", username);
    }

    let volume = localStorage.getItem("volume");
    if (volume) {
        volumeBar.value = volume;
        player.setVolume(volume);
    } else {
        player.setVolume(volumeBar.value);
    }

    messageBox.addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            let msg = messageBox.value;

            if (!msg) {
                return;
            }

            messageBox.value = "";
            send(socket, id, "message", `${username ? username : "guest"}: ${msg}`);
        }
    });

    volumeUpBtn.addEventListener("click", function () {
        player.setVolume(0);
        volumeBar.value = 0;

        volumeUpBtn.style.display = "none";
        volumeMuteBtn.style.display = "inline";
    });

    volumeMuteBtn.addEventListener("click", function () {
        player.setVolume(lastVolume);
        volumeBar.value = lastVolume;

        volumeMuteBtn.style.display = "none";
        volumeUpBtn.style.display = "inline";
    });

    volumeBar.addEventListener("input", function (e) {
        lastVolume = e.target.value;
        localStorage.setItem("volume", e.target.value);
        player.setVolume(e.target.value);
    });

    progressBar.addEventListener("mouseup", function (e) {
        let newTime = player.getDuration() * (e.target.value / 100);

        send(socket, id, "seek", newTime.toString());
    });

    playBtn.addEventListener("click", function () {
        let newTime = player.getDuration() * (progressBar.value / 100);
        send(socket, id, "play", newTime.toString());

        playBtn.style.display = "none";
        pauseBtn.style.display = "inline";
    });

    pauseBtn.addEventListener("click", function () {
        let newTime = player.getDuration() * (progressBar.value / 100);
        send(socket, id, "pause", newTime.toString());

        pauseBtn.style.display = "none";
        playBtn.style.display = "inline";
    });

    document.addEventListener("click", function (event) {
        if (event.target != document.querySelector("#search-results div") && searchResults.style.display != "none") {
            if (event.target == document.querySelector("#video-search")) {
                return;
            }

            searchResults.style.display = "none";
        }

        if (event.target != roomNameBtn && event.target != roomName && event.target.tagName != "path" && roomName.contentEditable == "true") {
            roomName.contentEditable = false;
            send(socket, id, "room name update", roomName.textContent);
        }
    });

    videoSearchBtn.addEventListener("click", function () {
        searchResults.innerHTML = "";

        let q = videoQuery.value;

        let match = q.match(/(?:https?:\/\/)?(?:www\.|m\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\/?\?v=|\/embed\/|\/shorts\/|\/)([^\s&\?\/\#]+)/);
        if (match) {
            send(socket, id, "video update", match[1]);
            return;
        }

        let cachedItems = searchCache.get(q);

        if (cachedItems) {
            listItems(cachedItems, socket);
            return;
        }

        lastSearch = q;

        if (!q) {
            return;
        }

        send(socket, id, "video search", q);
    });

    videoQuery.addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            searchResults.innerHTML = "";

            let q = videoQuery.value;

            let match = q.match(/(?:https?:\/\/)?(?:www\.|m\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\/?\?v=|\/embed\/|\/shorts\/|\/)([^\s&\?\/\#]+)/);
            if (match) {
                send(socket, id, "video update", match[1]);
                return;
            }

            let cachedItems = searchCache.get(q);

            if (cachedItems) {
                listItems(cachedItems, socket);
                return;
            }

            lastSearch = q;

            if (!q) {
                return;
            }

            send(socket, id, "video search", q);
        }
    });

    chatUpdates.addEventListener("click", function () {
        let status = document.getElementsByClassName("status");

        if (localStorage.getItem("hide chat updates") == "true") {
            for (let item of status) {
                item.style.display = "flex";
            }
            localStorage.setItem("hide chat updates", "false");
        } else {
            for (let item of status) {
                item.style.display = "none";
            }
            localStorage.setItem("hide chat updates", "true");
        }
    });

    videoPermBtn.addEventListener("click", function () {
        send(socket, id, "room permission update", "video");
    });

    timePermBtn.addEventListener("click", function () {
        send(socket, id, "room permission update", "seek");
    });

    playpausePermBtn.addEventListener("click", function () {
        send(socket, id, "room permission update", "playpause");
    });

    settingsBtn.addEventListener("click", function () {
        if (chatSettings.style.display == "none" || !chatSettings.style.display) {
            usernameInput.value = username ? username : "";
            chatSettings.style.display = "flex";
        } else {
            chatSettings.style.display = "none";
        }
    });

    settingsSaveBtn.addEventListener("click", function () {
        username = usernameInput.value;

        if (!username) {
            chatSettings.style.display = "none";
            return;
        }

        usernameInput.value = "";
        chatSettings.style.display = "none";
        localStorage.setItem("username", username);
        send(socket, id, "name update", username);
    });

    usernameInput.addEventListener('keypress', function (e) {
        if (e.key === 'Enter') {
            username = usernameInput.value;

            if (!username) {
                chatSettings.style.display = "none";
                return;
            }

            usernameInput.value = "";
            chatSettings.style.display = "none";
            localStorage.setItem("username", username);
            send(socket, id, "name update", username);
        }
    });

    roomNameBtn.addEventListener("click", function () {
        if (my_id != owner) {
            return;
        }

        roomName.contentEditable = true;
        roomName.focus();
        document.execCommand('selectAll', false, null);
    });

    roomName.addEventListener('keydown', (evt) => {
        if (evt.keyCode === 13) {
            roomName.contentEditable = false;
            send(socket, id, "room name update", roomName.textContent);
            evt.preventDefault();
        }
    });
}

function send(socket, id, event, message) {
    socket.send(
        JSON.stringify({
            id: id,
            event: event,
            message: message,
            timestamp: new Date().getTime(),
            uid: my_id ? Number(my_id) : 0,
        })
    );
}

function listItems(items, socket) {
    searchCache.set(lastSearch, items);
    let searchResults = document.querySelector("#search-results");
    searchResults.style.display = "flex";
    items.forEach(item => {
        let div = document.createElement("div");
        let img = document.createElement("img");
        let span = document.createElement("span");

        img.src = item.snippet.thumbnails["high"].url;

        div.addEventListener("click", function () {
            searchResults.style.display = "none";
            send(socket, id, "video update", item.id.videoId);
        });

        span.textContent = decodeHtml(item.snippet.title);
        searchResults.appendChild(div);
        div.append(img);
        div.append(span);
    });
}

function addMessage(msg) {
    let chatBox = document.querySelector("#chat-box");
    let div = document.createElement("div");
    let time = document.createElement("span");
    let message = document.createElement("span");

    div.classList.add("message");
    time.textContent = `[${new Date().toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })}] `
    time.classList.add("time");
    message.textContent = msg;
    div.appendChild(time);
    div.appendChild(message);
    chatBox.appendChild(div);

    if (chatBox.scrollTop + chatBox.clientHeight >= chatBox.scrollHeight - 100) {
        chatBox.scrollTop = chatBox.scrollHeight;
    }
}

function addStatusMessage(msg) {
    let chatBox = document.querySelector("#chat-box");
    let div = document.createElement("div");
    let time = document.createElement("span");
    let message = document.createElement("span");

    div.classList.add("status");
    time.textContent = `[${new Date().toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })}] `
    time.classList.add("time");
    message.innerHTML = msg;
    div.appendChild(time);
    div.appendChild(message);

    if (localStorage.getItem("hide chat updates") == "true") {
        div.style.display = "none";
    }

    chatBox.appendChild(div);

    if (chatBox.scrollTop + chatBox.clientHeight >= chatBox.scrollHeight - 100) {
        chatBox.scrollTop = chatBox.scrollHeight;
    }
}

function addUsers(newUsers, socket) {
    let usersbox = document.querySelector("#users");
    users = newUsers;

    for (const [key, value] of Object.entries(newUsers)) {
        if (document.querySelector(`#users div[title='${key}']`)) {
            continue;
        }

        let user = document.createElement("div");
        let name = document.createElement("span");
        let state = document.createElement("span");
        user.title = key;

        user.addEventListener("click", function () {
            send(socket, id, "owner update", user.uid);
        });

        user.uid = key;

        if (key == my_id) {
            user.classList.add("me");
        }

        name.textContent = value.name ? value.name : "Guest";
        user.appendChild(name);
        user.appendChild(state);
        usersbox.appendChild(user);
    }
}

function updateUser(user, id) {
    let userItem = document.querySelector(`#users div[title='${id}']`);
    userItem.firstElementChild.textContent = user.name;
    users[id].name = user.name;
}

function rmUser(id) {
    let userItem = document.querySelector(`#users div[title='${id}']`);
    userItem.remove();
    delete users[id];
}

function displayUserState(user, id) {
    let userItem = document.querySelector(`#users div[title='${id}']`);

    let state;
    switch (user.state) {
        case -1:
            state = "unstarted"
            break;
        case 0:
            state = "ended"
            break;
        case 1:
            state = "playing"
            break;
        case 2:
            state = "paused"
            break;
        case 3:
            state = "buffering"
            break;
        case 5:
            state = "video cued"
            break;
    }

    userItem.lastElementChild.textContent = ` is ${state}`;
}

function decodeHtml(html) {
    let txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

function log(msg = "hello there") {
    console.log(`info ~ ${msg}`);
}

function playpause(state) {
    let playBtn = document.querySelector("#play-icon");
    let pauseBtn = document.querySelector("#pause-icon");

    switch (state) {
        case "pause":
            playBtn.style.display = "none";
            pauseBtn.style.display = "inline";
            break;

        case "play":
            pauseBtn.style.display = "none";
            playBtn.style.display = "inline";
            break;

        default:
            playBtn.style.display = "none";
            pauseBtn.style.display = "inline";
            break;
    }
}

function getName(uid) {
    if (!users[uid] || !users[uid].name) {
        return "Guest";
    }

    return users[uid].name;
}

function resize() {
    let w;
    let h;

    let ar = 16 / 9;
    let ratio = 1.35;

    if (window.innerWidth <= 1000) {
        ratio = 1.05;
    }

    w = window.innerWidth / ratio;
    h = w / ar;

    if (window.innerWidth >= 1700) {
        w = 1280;
        h = 720;
    }
    document.getElementsByClassName("grid")[0].style.gridTemplateRows = `56px ${h}px 26px minmax(120px, 1fr)`

    player.setSize(w, h);
}

window.onresize = resize;
