let player,
    time_update_interval,
    paused;

function onYouTubeIframeAPIReady() {
    let w;
    let h;

    let ar = 16 / 9;
    let ratio = 1.35;

    if (window.screen.width <= 1000) {
        ratio = 1.05;
    }

    w = window.screen.width / ratio;
    h = window.screen.width / ar / ratio;

    if (window.screen.width >= 1700) {
        w = 1280;
        h = 720;
    }

    document.getElementsByClassName("grid")[0].style.gridTemplateRows = `56px ${h}px 26px minmax(120px, 1fr)`

    player = new YT.Player("player", {
        width: w,
        height: h,
        videoId: "",
        playerVars: {
            color: "white",
            controls: 0,
            disablekb: 1,
            modestbranding: 1,
            showinfo: 0,
            rel: 0,
            cc_load_policy: 0,
            iv_load_policy: 3,
            playsinline: 1,
            enablecastapi: 0,
            showinfo: 0,
            html5: 1,
            forcenewui: 1,
            autoplay: 1,
        },
        events: {
            onReady: initialize,
        }
    });
}

function initialize() {
    ws();

    updateProgressBar();
    clearInterval(time_update_interval);
    time_update_interval = setInterval(function () {
        updateProgressBar();
    }, 1000);
}

function setVideo(id, time = 0) {
    player.loadVideoById(id, time);
}

function updateProgressBar() {
    let progressBar = document.querySelector("#progress-bar");
    progressBar.value = ((player.getCurrentTime() / player.getDuration()) * 100);
}
