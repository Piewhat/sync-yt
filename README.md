# sync-yt

### Current Features

- synced play/pause/time
- youtube search
- chat
- room permissions/owner/name
- viewer count/list
- flexible layout

## Testing

Get a YouTube api key at https://developers.google.com/youtube/v3/getting-started

```
git clone https://gitlab.com/Piewhat/sync-yt
cd sync-yt
YT=api_key cargo run
```

Navigate to http://127.0.0.1:3030/

## Web Usage

1. Change WebSocket address in app.js to `wss://example.com/chat`
2. Docker build and run with the YT environment variable
3. Setup like any other web service, reverse proxy, etc.
